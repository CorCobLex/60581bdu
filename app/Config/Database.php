<?php

namespace Config;

use CodeIgniter\Database\Config;

/**
 * Database Configuration
 */
class Database extends Config
{
	/**
	 * The directory that holds the Migrations
	 * and Seeds directories.
	 *
	 * @var string
	 */
	public $filesPath = APPPATH . 'Database' . DIRECTORY_SEPARATOR;

	/**
	 * Lets you choose which connection group to
	 * use if no other is specified.
	 *
	 * @var string
	 */
	public $defaultGroup = 'default';

	/**
	 * The default database connection.
	 *
	 * @var array
	 */
	public $default = [
        'DSN'      => 'pgsql:host=ec2-54-155-226-153.eu-west-1.compute.amazonaws.com;port=5432;dbname=depffp0095pa5c;user=kbbnpmlytvedsc;password=88e0189d7d11c59506b541a0760b9746181336f15f9ae01c9dd3764bbcb5e091',
        //'DSN'      => 'Postgre://kbbnpmlytvedsc:88e0189d7d11c59506b541a0760b9746181336f15f9ae01c9dd3764bbcb5e091@ec2-54-155-226-153.eu-west-1.compute.amazonaws.com:5432/depffp0095pa5c',
        'hostname' => '',//'ec2-54-155-226-153.eu-west-1.compute.amazonaws.com',
        'username' => '',//'kbbnpmlytvedsc',
        'password' => '',//'88e0189d7d11c59506b541a0760b9746181336f15f9ae01c9dd3764bbcb5e091',
        'database' => '',//'depffp0095pa5c',
        'DBDriver' => 'Postgre',
        'DBPrefix' => '',
        'pConnect' => TRUE,
        'DBDebug' => (ENVIRONMENT !== 'production'),
        'DBDebug'  => TRUE,
        'charset'  => 'utf8',
        'DBCollat' => 'utf8_general_ci',
        'swapPre'  => '',
        'encrypt'  => false,
        'compress' => false,
        'strictOn' => false,
        'failover' => [],
        'port'     => 5432
	];

	/**
	 * This database connection is used when
	 * running PHPUnit database tests.
	 *
	 * @var array
	 */
	public $tests = [
		'DSN'      => '',
		'hostname' => '127.0.0.1',
		'username' => '',
		'password' => '',
		'database' => ':memory:',
		'DBDriver' => 'SQLite3',
		'DBPrefix' => 'db_',  // Needed to ensure we're working correctly with prefixes live. DO NOT REMOVE FOR CI DEVS
		'pConnect' => false,
		'DBDebug'  => (ENVIRONMENT !== 'production'),
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		'port'     => 3306,
	];

	//--------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();

		// Ensure that we always set the database group to 'tests' if
		// we are currently running an automated test suite, so that
		// we don't overwrite live data on accident.
		if (ENVIRONMENT === 'testing')
		{
			$this->defaultGroup = 'tests';
		}
	}

	//--------------------------------------------------------------------

}
