<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public $server;

    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $storage = new MyPdo(array('dsn' => 'pgsql:host=ec2-54-155-226-153.eu-west-1.compute.amazonaws.com;port=5432;dbname=depffp0095pa5c;user=kbbnpmlytvedsc;password=88e0189d7d11c59506b541a0760b9746181336f15f9ae01c9dd3764bbcb5e091;'),array('user_table' => 'users'));
        $grantType = new UserCredentials($storage);
        $this->server = new Server($storage);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}
