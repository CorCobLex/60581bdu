<?php namespace App\Models;
use CodeIgniter\Model;
class InstModel extends Model
{
    protected $table = 'inst'; //таблица, связанная с моделью
    protected $allowedFields = [ 'id_group','instmodel', 'Wear_rate', 'Production_year', 'Manufacture_country', 'price', 'fabricator', 'picture_url'];


    public function getInst($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }


    public function getInsts($id = null, $search = '', $per_page = null)
    {
        $model = $this->like('instmodel', is_null($search) ? '' : $search, 'both');
        if (!is_null($id)) {
            //$model = $model->where('id', $id);
        }

        // Пагинация
        return $model->paginate($per_page, 'group1');
    }

    public function getInstWithInster($id = null, $search = '')
    {
        $builder = $this->select('*')->join('Type','inst.id_group = Type.id')->like('instmodel', $search,'both', null, true)->orlike('fabricator',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['inst.id' => $id])->first();
        }
        return $builder;
    }
}
