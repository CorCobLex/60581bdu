<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Inst extends Migration
{
    public function up()
    {

        if(!$this->db->tableExists('Type')) {
            $this->db->query("CREATE TABLE `Type` (
                `id` int NOT NULL COMMENT 'id группы',
                `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'название группы',
                PRIMARY KEY (`id`)
                

                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;");
        }

        if (!$this->db->tableexists('inst')) {
            $this->db->query("CREATE TABLE `inst` (
                `id` int NOT NULL COMMENT 'id предмета',
                `id_group` int DEFAULT NULL COMMENT 'айди группы',
                `Model` varchar(255) NOT NULL COMMENT 'модель',
                `Wear_rate` double NOT NULL COMMENT 'Степень износа инструмента',
                `Manufacture_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'Страна производства',
                `price` int NOT NULL COMMENT 'цена за аренду',
                `fabricator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'Производитель',
                PRIMARY KEY (`id`),
                KEY `id_group` (`id_group`),
                CONSTRAINT `inst_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `Type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
                ");

        }





        if(!$this->db->tableExists('Rent')) {
            $this->db->query("CREATE TABLE `Rent` (
              `id_rent` int NOT NULL,
              `value` int NOT NULL,
              `count_days` int NOT NULL,
              `id_product` int NOT NULL,
              `date_take` date NOT NULL,
              `id_user` int NOT NULL,
              `return_plan` date NOT NULL,
              `return_fact` date NOT NULL,
              PRIMARY KEY (`id_product`),
              KEY `id_user` (`id_user`)
              
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;");
        }

    }

    public function down()
    {
        $this->forge->droptable('inst');
        $this->forge->droptable('Type');

        $this->forge->droptable('Rent');
    }
}
