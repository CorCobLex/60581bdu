<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use PhpParser\Comment;

class Inst extends Migration
{
    public function up()
    {

        if(!$this->db->tableExists('Type')) {


            $this->forge->addkey('id', TRUE);


            $this->forge->addfield(array(
            'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => FALSE),
            'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),

            ));

            // create table
            $this->forge->createtable('Type', TRUE);

        }

        if (!$this->db->tableexists('inst')) {

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_group' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => TRUE),
                'Model' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'Wear_rate' => array('type' => 'FLOAT', 'unsigned' => TRUE, 'null' => FALSE),
                'Manufacture_country' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'price' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'fabricator' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),

            ));
            $this->forge->addForeignKey('id_group','Type','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('inst', TRUE);





        }





        if(!$this->db->tableExists('Rent')) {

            $this->forge->addfield(array(
                'id_rent' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'value' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'count_days' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_product' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'date_take' => array('type' => 'DATE', 'null' => FALSE),
                'id_user' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'return_plan' => array('type' => 'DATE', 'null' => FALSE),
                'return_fact' => array('type' => 'DATE', 'null' => FALSE),
            ));

            $this->forge->createtable('Rent', TRUE);

    }

    }

    public function down()
    {
        $this->forge->droptable('inst');
        $this->forge->droptable('Type');

        $this->forge->droptable('Rent');
    }
}
