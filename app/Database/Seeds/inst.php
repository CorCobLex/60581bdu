<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class inst extends Seeder
{
    public function run()
    {

        $this->db->query("INSERT INTO \"Type\" (\"id\", \"name\") VALUES
            (1, 'Шуруповерты'),
            (2, 'Станки'),
            (3, 'Молотки'),
            (4, 'Машинки'),
            (5, 'Дрели'),
            (6, 'Остальное');");


        $this->db->query("INSERT INTO \"inst\" (\"id\", \"id_group\", \"Model\", \"Wear_rate\", \"Manufacture_country\", \"price\", \"fabricator\") VALUES
    (1, 1, 'SHURUPOVERT-3000', 1, 'DEUTCH', 7000, 'AMERICANS'),
    (2, 2, 'STAR PLATINUM', 1, 'JAPAN', 100000, 'JoJo'),
    (3, 4, 'Danon', 1, 'Mexica', 5000, 'Mexicans'),
    (4, 3, 'TUC-TUC', 1, 'Russia', 700, 'Ivan Golunov'),
    (5, 6, 'Zaichick', 1, 'Gvatemala', 10, 'Zaichiki'),
    (6, 5, 'Drobilka', 1, 'Australia', 10500, 'Man-o-Factory');");


        }
}