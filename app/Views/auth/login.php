<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container mt-5 pt-5 mb-5 pb-5 bg-light border mb-3 shadow-sm rounded">
    <div class="row justify-content-center text-center">
        <div class="col-md-6 col-12">
            <h1>Авторизация</h1>
            <p>Введите ваш логин и пароль</p>

            <?php if (isset($message)): ?>
                <div class="alert alert-danger text-center">Ошибка авторизации</div>
            <?php endif ?>

            <?php echo form_open('auth/login');?>
            <div class="mb-3">
                Логин:
                <?php echo form_input($identity, '','class="form-control" value="Mark" required');?>
            </div>
            <div class="mb-3">
                Пароль:
                <?php echo form_input($password, '','class="form-control" value="Mark" required');?>
            </div>
            <span class="mb-3">
                    Запомнить меня
                    <?php echo form_checkbox('remember', '1', false, 'id="remember"');?>
                </span>
            <div class="mb-3 row justify-content-center">
                <?php echo form_submit('submit', lang('Войти'), 'class="btn btn-primary"');?>
            </div>
            <?php echo form_close();?>
        </div>
    </div>

    <div class="text-center mb-3"><a class="text-decoration-none text-light p-2 rounded btn-dark" href="forgot_password"><?php echo lang('Забыли пароль');?></a></div>
    <div class="text-center mb-3"><a class="text-decoration-none text-light p-2 rounded btn-dark" href="register_user"><?php echo lang('Зарегестрироваться');?></a></div>
    <a href="<?= $authUrl; ?>" class="btn btn-outline-dark" role="button" style="text-transform:none">
        <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />

    </a>
</div>

<?= $this->endSection() ?>
