<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все Инструменты</h2>

<?php if (!empty($inst) && is_array($inst)) : ?>

    <?php foreach ($inst as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">

                        <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['instmodel']); ?>">



                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['instmodel']); ?></h5>
                        <p class="card-text"><?= esc($item['fabricator']); ?></p>
                        <a href="<?= base_url()?>/index.php/inst/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                        <p class="card-text"><small class="text-muted"><?= esc($item['price']); ?></small></p>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
<?php else : ?>
    <p>Невозможно найти иструменты</p>
<?php endif ?>
</div>
<?= $this->endSection() ?>
