<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('inst/store'); ?>
        <div class="form-group">
            <label for="Model">Модель</label>
            <input type="text" class="form-control <?= ($validation->hasError('instmodel')) ? 'is-invalid' : ''; ?>" name="instmodel"
                   value="<?= old('instmodel'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('instmodel') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="Model">Производитель</label>
            <input type="text" class="form-control <?= ($validation->hasError('fabricator')) ? 'is-invalid' : ''; ?>" name="fabricator"
                   value="<?= old('fabricator'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('fabricator') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="Model">Цена</label>
            <input type="number" class="form-control <?= ($validation->hasError('price')) ? 'is-invalid' : ''; ?>" name="price"
                   value="<?= old('price'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('price') ?>
            </div>

        </div>

        <div class="form-group">
            <label for="Model">Степень износа</label>
            <input type="number" class="form-control <?= ($validation->hasError('Wear_rate')) ? 'is-invalid' : ''; ?>" name="Wear_rate"
                   value="<?= old('Wear_rate'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Wear_rate') ?>
            </div>

        </div>


        <div class="form-group">
            <label for="Model">Страна производства</label>
            <input type="text" class="form-control <?= ($validation->hasError('Manufacture_country')) ? 'is-invalid' : ''; ?>" name="Manufacture_country"
                   value="<?= old('Manufacture_country'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Manufacture_country') ?>
            </div>

        </div>

        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>