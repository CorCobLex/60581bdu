
<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">

    <?php if (!empty($inst) && is_array($inst)) : ?>
        <h2>Редактор инструментов</h2>
        <div class="d-flex justify-content-between mb-2">
            <?= $pager->links('group1', 'my_page') ?>
            <?= form_open('inst/viewAllWithInst', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('inst/viewAllWithInst',['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Название или описание" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
            </form>
        </div>
        <table class="table table-striped">
            <thead>

            <th scope="col">Картинка</th>
            <th scope="col">Название</th>
            <th scope="col">Степень износа</th>
            <th scope="col">Производитель</th>
            <th scope="col">Управление</th>
            <th scope="col">Цена</th>

            </thead>
            <tbody>
            <?php foreach ($inst as $item): ?>
                <tr>
                    <td>
                        <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['instmodel']); ?>">                    </td>
                    <td><?= esc($item['instmodel']); ?></td>
                    <td><?= esc($item['Wear_rate']); ?></td>
                    <td><?= esc($item['fabricator']); ?></td>
                    <td>
                        <a href="<?= base_url()?>/inst/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                        <a href="<?= base_url()?>/inst/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url()?>/inst/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                    </td>
                    <td><?= esc($item['price']); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php else : ?>
        <div class="text-center">
            <p>Инструменты не найдены!</p>
            <a class="btn btn-primary btn-lg" href="<?= base_url()?>/inst/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать инструмент</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>