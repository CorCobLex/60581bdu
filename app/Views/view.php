<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($inst)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <img height="150" src="<?= esc($inst['picture_url']); ?>" class="card-img" alt="<?= esc($inst['instmodel']); ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($inst['instmodel']); ?></h5>
                            <p class="card-text"><?= esc($inst['fabricator']); ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Тип:</div>
                                <?php switch($inst['id_group']) {
                                    case 1: ?>
                                        <div class="text-muted">Шуруповерты</div>
                                        <?php break;
                                    case 2: ?>
                                        <div class="text-muted">Станки</div>
                                        <?php break;
                                    case 3: ?>
                                        <div class="text-muted">Молотки</div>
                                        <?php break;
                                    case 4: ?>
                                        <div class="text-muted">Машинки</div>
                                        <?php break;
                                    case 5: ?>
                                        <div class="text-muted">Дрели</div>
                                        <?php break;
                                    case 6: ?>
                                        <div class="text-muted">Остальное</div>
                                        <?php break;
                                    default: ?>
                                        <div class="text-muted">Неизвестно</div>
                                        <?php break;
                                }?>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Степень износа</div>
                                <div class="text-muted"><?= esc($inst['Wear_rate']); ?></div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Цена:</div>
                                <div class="text-muted"><?= esc($inst['price']); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
