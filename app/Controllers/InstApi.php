<?php

namespace App\Controllers;

use Aws\S3\S3Client;
use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;
use App\Models\InstModel;
use App\Models\OAuthModel;


class InstApi extends ResourceController
{
    protected $modelName = 'App\Models\InstModel';
    protected $format = 'json';
    protected $oauth;

    public function __construct()
    {
        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }
    public function inst() //Отображение всех записей
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $data = $this->model->getInsts($OAuthModel->getUser()->group_name == 'admin' ? null : $OAuthModel->getUser()->id, $search, $per_page);

            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['insts' => $data, 'pager' => $model->pager->getDetails('group1')]);
        } else $this->oauth->server->getResponse()->send();
    }

    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            //$OAuthModel = new OAuthModel();
            $model = $this->model;


            if ($this->request->getMethod() === 'post' && $this->validate([
                    'instmodel' => 'required|min_length[3]|max_length[255]',
                    'fabricator'  => 'required',
                    'price'  => 'required',
                    'Wear_rate'  => 'required',
                    'Manufacture_country'  => 'required',
                    'picture'  => 'is_image[picture]|max_size[picture,12000]',
                ]))
            {
                $insert = null;

                $file = $this->request->getFile('picture');
                if ($file->getSize() != 0) {


                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region' => 'us-east-1',
                        'endpoint' => getenv('S3_ENDPOINT'),
                        'use_path_style_endpoint' => true,
                        'credentials' => [
                            'key' => getenv('S3_KEY'),
                            'secret' => getenv('S3_SECRET'),
                        ],
                    ]);


                    $ext = explode('.', $file->getName());
                    $ext = $ext[count($ext) - 1];

                    $insert = $s3->putObject([
                        'Bucket' => getenv('S3_BUCKET'),

                        'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                        'Body' => fopen($file->getRealPath(), 'r+'),

                    ]);

                }



                $new_data = [
                    'instmodel' => $this->request->getPost('instmodel'),
                    'fabricator' => $this->request->getPost('fabricator'),
                    'price' => $this->request->getPost('price'),
                    'Wear_rate' => $this->request->getPost('Wear_rate'),
                    'Manufacture_country' => $this->request->getPost('Manufacture_country'),
                ];

                if (!is_null($insert))
                    $new_data['picture_url'] = $insert['ObjectURL'];
                else{
                    $model->save($new_data);
                    return $this->respondCreated(null, 'S3 Link error. Empty poster url. Inst created successfully');
                }

                $model->save($new_data);
                //-----------------------------
                return $this->respondCreated(null, 'Inst created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();
    }

    /*
    public function store()
    {
        helper(['form','url']);

        $this->oauth = new OAuth();

        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;

        if ($this->request->getMethod() === 'post' && $this->validate([
                'instmodel' => 'required|min_length[3]|max_length[255]',
                'fabricator'  => 'required',
                'price'  => 'required',
                'Wear_rate'  => 'required',
                'Manufacture_country'  => 'required',
                //'picture'  => 'is_image[picture]|max_size[picture,1024]',

            ]))
        {

            $model = new InstModel();
            $data = [
                'instmodel' => $this->request->getPost('instmodel'),
                'fabricator' => $this->request->getPost('fabricator'),
                'price' => $this->request->getPost('price'),
                'Wear_rate' => $this->request->getPost('Wear_rate'),
                'Manufacture_country' => $this->request->getPost('Manufacture_country'),

            ];

            $model->save($data);
            return $this->respondCreated(null, 'Inst created successfully');
        } else {
            return $this->respond($this->validator->getErrors());
        }
        } else $this->oauth->server->getResponse()->send();
    }
*/

    public function delete($id = null)
    {
        //delete
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $model = new InstModel();
            $model->delete($id);
            return $this->respondDeleted(null, 'Inst deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }

    /*
    //старая
    public function inst() //Отображение всех записей
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            return $this->respond($this->model->getInst());

        }
        $oauth->server->getResponse()->send();
    }
    */

}