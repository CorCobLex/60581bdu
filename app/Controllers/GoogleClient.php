<?php namespace App\Controllers;

use Google_Client;
use function base_url;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('174518981424-pn3f3rbtk5g1btgef55ovtkela4qchru.apps.googleusercontent.com');
        $this->google_client->setClientSecret('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');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}