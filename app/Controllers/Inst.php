<?php namespace App\Controllers;

use App\Models\InstModel;
use Config\Services;
use Aws\S3\S3Client;

class inst extends BaseController


{
 public function index() //Обображение всех записей
 {
     //если пользователь не аутентифицирован - перенаправление на страницу входа
     if (!$this->ionAuth->loggedIn())
     {
         return redirect()->to('/auth/login');
     }
   $model = new InstModel();
   $data ['inst'] = $model->getInst();
   echo view('view_all', $this->withIon($data));
 }

 public function view($id = null) //отображение одной записи
 {
     //если пользователь не аутентифицирован - перенаправление на страницу входа
     if (!$this->ionAuth->loggedIn())
     {
         return redirect()->to('/auth/login');
     }
   $model = new InstModel();
   $data ['inst'] = $model->getInst($id);
   echo view('view', $this->withIon($data));
 }

    public function viewAllWithInst()
    {

        if ($this->ionAuth->isAdmin())
        {
            $perPage = 0;
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new InstModel();
            $data['inst'] = $model->getInstWithInster(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;

            echo view('view_all_with_users', $this->withIon($data));
        }
        else
        {
            //session()->setFlashdata('message', lang('Inst.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {

        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = Services::validation();
        echo view('create', $this->withIon($data));
    }


    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'instmodel' => 'required|min_length[3]|max_length[255]',
                'fabricator'  => 'required',
                'price'  => 'required',
                'Wear_rate'  => 'required',
                'Manufacture_country'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',

            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'ACL' => 'public-read',
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new InstModel();
            $data = [
                'instmodel' => $this->request->getPost('instmodel'),
                'fabricator' => $this->request->getPost('fabricator'),
                'price' => $this->request->getPost('price'),
                'Wear_rate' => $this->request->getPost('Wear_rate'),
                'Manufacture_country' => $this->request->getPost('Manufacture_country'),

            ];

            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            //session()->setFlashdata('message', lang('Inst.inst_create_success'));
            return redirect()->to('/inst');
        }
        else
        {
            return redirect()->to('/inst/create')->withInput();
        }
    }


    public function edit($id)
    {

        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }



        $model = new InstModel();

        helper(['form']);
        $data ['inst'] = $model->getInst($id);

        $data ['validation'] =  Services::validation();
        echo view('edit', $this->withIon($data));

        var_dump($data['inst']);

    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new InstModel();
        $model->delete($id);
        return redirect()->to('/inst/viewAllWithInst');
    }

    public function update()
    {
        helper(['form','url']);
        echo '/inst/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'model' => 'required|min_length[3]|max_length[255]',
                'fabricator'  => 'required',
                'price'  => 'required',
                'Wear_rate'  => 'required',
                'Manufacture_country'  => 'required',
                
            ]))
        {
            $model = new InstModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'model' => $this->request->getPost('model'),
                'fabricator' => $this->request->getPost('fabricator'),
                'price' => $this->request->getPost('price'),
                'Wear_rate' => $this->request->getPost('Wear_rate'),
                'Manufacture_country' => $this->request->getPost('Manufacture_country'),

            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/inst');
        }
        else
        {
            return redirect()->to('/inst/edit/'.$this->request->getPost('id'))->withInput();
        }


    }









}


